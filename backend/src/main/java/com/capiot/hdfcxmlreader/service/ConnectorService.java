package com.capiot.hdfcxmlreader.service;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.capiot.hdfcxmlreader.model.Connector;
import com.capiot.hdfcxmlreader.model.TransformField;
import com.capiot.hdfcxmlreader.model.Transformation;

@Component
public class ConnectorService {
	
	@Autowired
	Environment environment;
	
	/**
	 * <p>Creating the JSON for updating the relation</p>
	 * @param connector
	 * @param relationMap
	 * @param transformations
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public JSONObject createJSONConnector(List<Connector> connector, Map<String, String> relationMap, List<Transformation> transformations) {
		JSONObject request = new JSONObject();
		
		request.put("name", connector.get(0).getFromInstance());
		request.put("description", null);
		request.put("api", "/"+connector.get(0).getFromInstance().replace("_", "").toLowerCase());
		request.put("domain", environment.getProperty("domain"));
		request.put("version", 1);
		
		JSONArray wizard = new JSONArray();
		request.put("wizard", wizard);
	
		
		/*Fetching all the fields from the Transformation matching the Connector*/
		List<TransformField> transformFields = new ArrayList<>();
		for(int index=0; index<transformations.size();index++) {
			if(transformations.get(index).getName().equalsIgnoreCase(connector.get(0).getFromInstance())) {
				transformFields = transformations.get(index).getTransformFields();
			}
		}
		
		JSONObject definition = new JSONObject();
		/**
		 * Adding all the fields under definition
		 */
		for(TransformField transformfield: transformFields) {
			JSONObject tempField = new JSONObject();		
			JSONObject properties = new JSONObject();
			JSONArray relatedViewFields = new JSONArray();
			JSONObject relatedViewField = new JSONObject();
			relatedViewField.put("_id", RandomStringUtils.randomAlphanumeric(24));
			relatedViewField.put("name", transformfield.getName().toUpperCase());
			relatedViewField.put("key", CommonService.toCamelCase(transformfield.getName()));
			relatedViewFields.add(relatedViewField);
			properties.put("name", transformfield.getName());
			properties.put("_typeChanged", "Relation");
			properties.put("relatedTo", relationMap.get(connector.get(0).getToInstance()));
			properties.put("relatedSearchField", CommonService.toCamelCase(transformfield.getName()));	
			properties.put("relatedViewFields", relatedViewFields);
			tempField.put("type", "Relation");
			tempField.put("properties", properties);
			definition.put(CommonService.toCamelCase(transformfield.getName()), tempField);
		}
		
		JSONObject properties = new JSONObject();
		properties.put("name", "stage-staging");
		properties.put("_typeChanged", "String");
		
		JSONObject stage_staging = new JSONObject();
		stage_staging.put("type", "String");
		stage_staging.put("properties", properties);
		definition.put(CommonService.toCamelCase("stage_staging"), stage_staging);
		
		
		/*Adding id*/
		JSONObject _id = new JSONObject();
		_id.put("prefix", connector.get(0).getFromInstance().toUpperCase().substring(0,3));
		_id.put("suffix", null);
		_id.put("padding", null);
		_id.put("counter", 1001);
		_id.put("isPermanentDelete", true);
		JSONObject propertiesOfId = new JSONObject();
		propertiesOfId.put("name", "ID");
		_id.put("properties", propertiesOfId);
		definition.put("_id", _id);
		
		
		/* Adding attributes */
		JSONArray attributeList = new JSONArray();
		for(TransformField transformfield:transformFields) {
			JSONObject tempAttribute = new JSONObject();
			tempAttribute.put("key", CommonService.toCamelCase(transformfield.getName())+"._id");
			tempAttribute.put("name", transformfield.getName());	
			attributeList.add(tempAttribute);
		}
		JSONObject idAttribute = new JSONObject();
		idAttribute.put("key", "_id");
		idAttribute.put("name", "ID");
		attributeList.add(idAttribute);
		
		JSONObject stageStagingAttribute = new JSONObject();
		stageStagingAttribute.put("key", CommonService.toCamelCase("stage_staging"));
		stageStagingAttribute.put("name", "stage-staging");
		attributeList.add(stageStagingAttribute);
		
		List<TransformField> transformFields2 = new ArrayList<>();
		TransformField transformField = new TransformField();
		transformField.setName(connector.get(0).getFromField());
		transformField.setDataType("String");
		transformFields2.add(transformField);
		
		
		JSONObject versionValidity = new JSONObject();
		versionValidity.put("validityType", "count");
		versionValidity.put("validityValue", -1);
		
		request.put("definition", definition);
		request.put("attributeList", attributeList);
		request.put("versionValidity", versionValidity);
		request.put("role", CommonService.createDefaultRolesForUpdate(connector.get(0).getFromInstance(), transformFields, connector));
		
		
		return request;
	}
}
