package com.capiot.hdfcxmlreader.service;


import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.capiot.hdfcxmlreader.exception.InvalidRequestException;
import com.fasterxml.jackson.core.JsonProcessingException;


@Service
@Configuration
@PropertySource("classpath:hdfc.properties")
public class ODPClient {
	
	@Autowired
	private Environment environment;
	@Autowired
	RestTemplate template;
	@Autowired
	TokenGenerateService tokenGenerateService;

	static String token = "someinvalidtokentostartwith";
	public static final Logger logger = LoggerFactory.getLogger(ODPClient.class);
	
	/**
	 * <p>Creating the Data service in ODP</p>
	 * @param request
	 * @return
	 * @throws InvalidRequestException
	 */
	public ResponseEntity<JSONObject> createDataService(JSONObject request) throws InvalidRequestException{
		final String serviceUrl = environment.getProperty("service_url");
		ResponseEntity<JSONObject> response = null;
		
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.set("Authorization", "JWT " + token);
		logger.info("URL: " + serviceUrl);

		START: while (response == null) {
			try {
				HttpEntity<JSONObject> entity = new HttpEntity<>(request, header);
				logger.info("Creating a Data Service.");
				long startTime = System.currentTimeMillis();
				response = template.exchange(serviceUrl, HttpMethod.POST, entity, JSONObject.class);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info("Time elapsed (in milliseconds) for creating the data service: "+elapsedTime);;
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					logger.info("Invalid JWT token. Generating a new token from Login.");
					try {
						token = tokenGenerateService.generateToken();
					} catch (JsonProcessingException | ParseException | InvalidRequestException e) {
						e.printStackTrace();
					}
					header.set("Authorization", "JWT " + token);
					continue START;
				} else {
					throw new InvalidRequestException(exception.getResponseBodyAsString(), exception);
				}
			}catch(HttpServerErrorException exception) {
				throw new InvalidRequestException(exception.getResponseBodyAsString(),exception);
			}
		}
		return response;
	}
	
	/**
	 * <p>Updating the Data Service in ODP</p>
	 * @param request
	 * @param id
	 * @return
	 * @throws InvalidRequestException
	 */
	public ResponseEntity<JSONObject> updateDataService(JSONObject request, String id) throws InvalidRequestException{
		final String serviceUrl = environment.getProperty("service_url");
		ResponseEntity<JSONObject> response = null;
		
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		header.set("Authorization", "JWT " + token);
		String updateUrl = serviceUrl+"/"+id;
		logger.info("URL for updating the relation: " + updateUrl);

		START: while (response == null) {
			try {
				HttpEntity<JSONObject> entity = new HttpEntity<>(request, header);
				logger.info("Creating the relation.");
				long startTime = System.currentTimeMillis();
				response = template.exchange(updateUrl, HttpMethod.PUT, entity, JSONObject.class);
				long elapsedTime = (System.currentTimeMillis() - startTime);
				logger.info("Time elapsed (in milliseconds) for creating the relation: "+elapsedTime);;
			} catch (HttpClientErrorException exception) {
				if (exception.getStatusText().contains("Unauthorized")) {
					logger.info("Invalid JWT token. Generating a new token from Login.");
					try {
						token = tokenGenerateService.generateToken();
					} catch (JsonProcessingException | ParseException | InvalidRequestException e) {
						e.printStackTrace();
					}
					header.set("Authorization", "JWT " + token);
					continue START;
				} else {
					throw new InvalidRequestException(exception.getResponseBodyAsString(), exception);
				}
			}catch(HttpServerErrorException exception) {
				throw new InvalidRequestException(exception.getResponseBodyAsString(),exception);
			}
		}
		return response;
	}
	
}
