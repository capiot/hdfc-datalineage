package com.capiot.hdfcxmlreader.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.capiot.hdfcxmlreader.model.Instance;
import com.capiot.hdfcxmlreader.model.TransformField;
import com.capiot.hdfcxmlreader.model.Transformation;

@Component
public class InstanceService {
	
	@Autowired
	Environment environment;
	
	@SuppressWarnings("unchecked")
	public JSONObject createInstance(Instance instance, List<Transformation> transformations) {
		JSONObject request = new JSONObject();
		
		request.put("name", instance.getName().toUpperCase());
		request.put("description", null);
		request.put("api", "/"+instance.getName().replace("_", "").toLowerCase());
		request.put("version", 1);
		request.put("domain", environment.getProperty("domain"));
		
		JSONObject definition = new JSONObject();
		Map<String,String> fieldNames = new HashMap<>();
		for(Transformation transformation: transformations) {
			List<TransformField> transformFields = transformation.getTransformFields();
			for(TransformField transformField: transformFields) {
				fieldNames.put(transformField.getName(), transformField.getDataType());
			}
		}	
		List<TransformField> transformFields = new ArrayList<>();
		fieldNames.forEach((k,v)->{
			TransformField temp = new TransformField();
			temp.setName(k);
			temp.setDataType(v);
			transformFields.add(temp);
		});
		for(TransformField transformfield: transformFields) {
			JSONObject tempField = new JSONObject();
			
			JSONObject properties = new JSONObject();
			properties.put("name", transformfield.getName());
			properties.put("_typeChanged", CommonService.changeDataType(transformfield.getDataType()));
			
			tempField.put("type", CommonService.changeDataType(transformfield.getDataType()));
			tempField.put("properties", properties);
			
			definition.put(CommonService.toCamelCase(transformfield.getName()), tempField);
		}
		JSONObject _id = new JSONObject();
		_id.put("prefix", instance.getName().toUpperCase().substring(0,3));
		_id.put("suffix", null);
		_id.put("padding", null);
		_id.put("counter", 1001);
		_id.put("isPermanentDelete", true);
		JSONObject propertiesOfId = new JSONObject();
		propertiesOfId.put("name", "ID");
		_id.put("properties", propertiesOfId);
		definition.put("_id", _id);
		
		if(instance.getName().equalsIgnoreCase("Shortcut_to_HEALTH_CHK_ERR_TBL")) {
			JSONObject properties = new JSONObject();
			properties.put("name", "stage-landing");
			properties.put("_typeChanged", "String");
			
			JSONObject landing = new JSONObject();
			landing.put("type", "String");
			landing.put("properties", properties);
			definition.put(CommonService.toCamelCase("stage_landing"), landing);
		}
		
		JSONArray attributeList = new JSONArray();
		fieldNames.forEach((k,v)->{
			JSONObject tempAttribute = new JSONObject();
			tempAttribute.put("key", CommonService.toCamelCase(k));
			tempAttribute.put("name", k);
			
			attributeList.add(tempAttribute);
		});
		JSONObject idAttribute = new JSONObject();
		idAttribute.put("key", "_id");
		idAttribute.put("name", "ID");
		attributeList.add(idAttribute);
		
		if(instance.getName().equalsIgnoreCase("Shortcut_to_HEALTH_CHK_ERR_TBL")) {
			JSONObject stageLanding = new JSONObject();
			stageLanding.put("key", CommonService.toCamelCase("stage_landing"));
			stageLanding.put("name", "stage-landing");
			attributeList.add(stageLanding);
		}
		
		JSONObject versionValidity = new JSONObject();
		versionValidity.put("validityType", "count");
		versionValidity.put("validityValue", -1);
		
		
		request.put("definition", definition);
		request.put("attributeList", attributeList);
		request.put("versionValidity", versionValidity);
		request.put("role", CommonService.createDefaultRoles(instance.getName(), transformFields));
		
		return request;
	}
}
