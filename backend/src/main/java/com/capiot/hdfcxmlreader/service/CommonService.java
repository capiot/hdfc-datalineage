package com.capiot.hdfcxmlreader.service;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.capiot.hdfcxmlreader.model.Connector;
import com.capiot.hdfcxmlreader.model.PowerMart;
import com.capiot.hdfcxmlreader.model.TransformField;


@Service
public class CommonService {
	
	public static final Logger logger = LoggerFactory.getLogger(CommonService.class);
	
	@Autowired
	Environment environment;
	
	
	/**
	 * @param File path of the xml
	 * @return Unmarshalled PowerMart Object
	 */
	public PowerMart unmarshalXML(String filePath) {
		try {
			JAXBContext context = JAXBContext.newInstance(PowerMart.class);
			XMLInputFactory xmlInFactory = XMLInputFactory.newFactory();
			xmlInFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
			XMLStreamReader xmlStream = xmlInFactory.createXMLStreamReader(new StreamSource(new File(filePath)));
			Unmarshaller unmarshaller = context.createUnmarshaller();
			return (PowerMart) unmarshaller.unmarshal(xmlStream);
		} catch (JAXBException e) {
			logger.debug("Unable to unmarshal the xml file");
			e.printStackTrace();
		} catch (XMLStreamException e) {
			logger.debug("Unable to create XML Stream reader for reading the xml file");
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * @param Input String
	 * @return String as CamelCase
	 */
	public static String toCamelCase(final String init) {
	    if (init==null)
	        return null;

	    final StringBuilder ret = new StringBuilder(init.length());
	    
	    String[] inputArray = init.split("_");
	    if (!inputArray[0].isEmpty()) {
            ret.append(inputArray[0].substring(0, 1).toLowerCase());
            ret.append(inputArray[0].substring(1).toLowerCase());
        }
	    for (int index=1; index<inputArray.length;index++) {
	        if (!inputArray[index].isEmpty()) {
	            ret.append(inputArray[index].substring(0, 1).toUpperCase());
	            ret.append(inputArray[index].substring(1).toLowerCase());
	        }
	        if (!(ret.length()==init.length()))
	            ret.append("");
	    }

	    return ret.toString();
	}
	
	
	
	@SuppressWarnings("unchecked")
	public static JSONObject createDefaultRoles(String dataServiceName, List<TransformField> transformFields) {
		JSONObject role = new JSONObject();
		JSONArray roles = new JSONArray();
		
		JSONObject skipReviewRole = new JSONObject();
		skipReviewRole.put("skipReviewRole", true);
		skipReviewRole.put("id", "P"+RandomStringUtils.randomNumeric(10));
		skipReviewRole.put("name", "Skip Review "+dataServiceName);
		skipReviewRole.put("operations", createSkipReviewOperations());
		skipReviewRole.put("description", "This role entitles an authorized user to create, update or delete a record without any approval");
		roles.add(skipReviewRole);
		
		JSONObject manageRole = new JSONObject();
		manageRole.put("manageRole", true);
		manageRole.put("id", "P"+RandomStringUtils.randomNumeric(10));
		manageRole.put("name", "Manage "+dataServiceName);
		manageRole.put("operations", createManageOperations());
		manageRole.put("description", "This role entitles an authorized user to create, update or delete a record");
		roles.add(manageRole);
		
		JSONObject viewRole = new JSONObject();
		viewRole.put("viewRole", true);
		viewRole.put("id", "P"+RandomStringUtils.randomNumeric(10));
		viewRole.put("name", "View "+dataServiceName);
		viewRole.put("operations", createViewOperations());
		viewRole.put("description", "This role entitles an authorized user to view the record");
		roles.add(viewRole);
		
		role.put("roles", roles);
		role.put("fields", createFields(dataServiceName,transformFields));
		return role;
	}
	
	
	@SuppressWarnings("unchecked")
	public static JSONObject createDefaultRolesForUpdate(String dataServiceName, List<TransformField> transformFields, List<Connector> connector) {
		JSONObject role = new JSONObject();
		JSONArray roles = new JSONArray();
		
		JSONObject skipReviewRole = new JSONObject();
		skipReviewRole.put("skipReviewRole", true);
		skipReviewRole.put("_id",RandomStringUtils.randomAlphanumeric(24));
		skipReviewRole.put("id", "P"+RandomStringUtils.randomNumeric(10));
		skipReviewRole.put("name", "Skip Review "+dataServiceName);
		skipReviewRole.put("operations", createSkipReviewOperationsForUpdate());
		skipReviewRole.put("description", "This role entitles an authorized user to create, update or delete a record without any approval");
		roles.add(skipReviewRole);
		
		JSONObject manageRole = new JSONObject();
		manageRole.put("manageRole", true);
		manageRole.put("_id",RandomStringUtils.randomAlphanumeric(24));
		manageRole.put("id", "P"+RandomStringUtils.randomNumeric(10));
		manageRole.put("name", "Manage "+dataServiceName);
		manageRole.put("operations", createManageOperationsForUpdate());
		manageRole.put("description", "This role entitles an authorized user to create, update or delete a record");
		roles.add(manageRole);
		
		JSONObject viewRole = new JSONObject();
		viewRole.put("viewRole", true);
		viewRole.put("_id",RandomStringUtils.randomAlphanumeric(24));
		viewRole.put("id", "P"+RandomStringUtils.randomNumeric(10));
		viewRole.put("name", "View "+dataServiceName);
		viewRole.put("operations", createViewOperationsForUpdate());
		viewRole.put("description", "This role entitles an authorized user to view the record");
		roles.add(viewRole);
		
		role.put("roles", roles);
		role.put("fields", createFieldsForUpdate(transformFields, connector));
		return role;
	}
	
	
	@SuppressWarnings("unchecked")
	public static JSONArray createSkipReviewOperations() {
		JSONArray operations = new JSONArray();
		
		JSONObject skipReview = new JSONObject();
		skipReview.put("workflowRoles", createEmptyWorkFlowRoles());
		skipReview.put("method", "SKIP_REVIEW");
		operations.add(skipReview);
		
		JSONObject get = new JSONObject();
		get.put("workflowRoles", createEmptyWorkFlowRoles());
		get.put("method", "GET");
		operations.add(get);
		
		JSONObject put = new JSONObject();
		put.put("workflowRoles", createEmptyWorkFlowRoles());
		put.put("method", "PUT");
		operations.add(put);
		
		JSONObject post = new JSONObject();
		post.put("workflowRoles", createEmptyWorkFlowRoles());
		post.put("method", "POST");
		operations.add(post);
		
		JSONObject delete = new JSONObject();
		delete.put("workflowRoles", createEmptyWorkFlowRoles());
		delete.put("method", "DELETE");
		operations.add(delete);
		
		return operations;
	}
	
	
	@SuppressWarnings("unchecked")
	public static JSONArray createManageOperations() {
		JSONArray operations = new JSONArray();
		
		JSONObject get = new JSONObject();
		get.put("workflowRoles", createEmptyWorkFlowRoles());
		get.put("method", "GET");
		operations.add(get);
		
		JSONObject put = new JSONObject();
		put.put("workflowRoles", createEmptyWorkFlowRoles());
		put.put("method", "PUT");
		operations.add(put);
		
		JSONObject post = new JSONObject();
		post.put("workflowRoles", createEmptyWorkFlowRoles());
		post.put("method", "POST");
		operations.add(post);
		
		JSONObject delete = new JSONObject();
		delete.put("workflowRoles", createEmptyWorkFlowRoles());
		delete.put("method", "DELETE");
		operations.add(delete);
		
		return operations;
	}
	
	
	@SuppressWarnings("unchecked")
	public static JSONArray createViewOperations() {
		JSONArray operations = new JSONArray();
		
		JSONObject get = new JSONObject();
		get.put("workflowRoles", createEmptyWorkFlowRoles());
		get.put("method", "GET");
		operations.add(get);
		
		return operations;
	}
	
	
	@SuppressWarnings("unchecked")
	public static JSONArray createSkipReviewOperationsForUpdate() {
		JSONArray operations = new JSONArray();
		
		JSONObject skipReview = new JSONObject();
		skipReview.put("workflowRoles", createEmptyWorkFlowRoles());
		skipReview.put("_id", RandomStringUtils.randomAlphanumeric(24));
		skipReview.put("method", "SKIP_REVIEW");
		operations.add(skipReview);
		
		JSONObject get = new JSONObject();
		get.put("workflowRoles", createEmptyWorkFlowRoles());
		get.put("_id", RandomStringUtils.randomAlphanumeric(24));
		get.put("method", "GET");
		operations.add(get);
		
		JSONObject put = new JSONObject();
		put.put("workflowRoles", createEmptyWorkFlowRoles());
		put.put("_id", RandomStringUtils.randomAlphanumeric(24));
		put.put("method", "PUT");
		operations.add(put);
		
		JSONObject post = new JSONObject();
		post.put("workflowRoles", createEmptyWorkFlowRoles());
		post.put("_id", RandomStringUtils.randomAlphanumeric(24));
		post.put("method", "POST");
		operations.add(post);
		
		JSONObject delete = new JSONObject();
		delete.put("workflowRoles", createEmptyWorkFlowRoles());
		delete.put("_id", RandomStringUtils.randomAlphanumeric(24));
		delete.put("method", "DELETE");
		operations.add(delete);
		
		return operations;
	}
	
	@SuppressWarnings("unchecked")
	public static JSONArray createManageOperationsForUpdate() {
		JSONArray operations = new JSONArray();
		
		JSONObject get = new JSONObject();
		get.put("workflowRoles", createEmptyWorkFlowRoles());
		get.put("_id", RandomStringUtils.randomAlphanumeric(24));
		get.put("method", "GET");
		operations.add(get);
		
		JSONObject put = new JSONObject();
		put.put("workflowRoles", createEmptyWorkFlowRoles());
		put.put("_id", RandomStringUtils.randomAlphanumeric(24));
		put.put("method", "PUT");
		operations.add(put);
		
		JSONObject post = new JSONObject();
		post.put("workflowRoles", createEmptyWorkFlowRoles());
		post.put("_id", RandomStringUtils.randomAlphanumeric(24));
		post.put("method", "POST");
		operations.add(post);
		
		JSONObject delete = new JSONObject();
		delete.put("workflowRoles", createEmptyWorkFlowRoles());
		delete.put("_id", RandomStringUtils.randomAlphanumeric(24));
		delete.put("method", "DELETE");
		operations.add(delete);
		
		return operations;
	}
	
	
	@SuppressWarnings("unchecked")
	public static JSONArray createViewOperationsForUpdate() {
		JSONArray operations = new JSONArray();
		
		JSONObject get = new JSONObject();
		get.put("workflowRoles", createEmptyWorkFlowRoles());
		get.put("_id", RandomStringUtils.randomAlphanumeric(24));
		get.put("method", "GET");
		operations.add(get);
		
		return operations;
	}
	
	
	public static JSONArray createEmptyWorkFlowRoles() {
		return new JSONArray();
	}
	

	@SuppressWarnings("unchecked")
	public static JSONObject createFields(String dataServiceName, List<TransformField> transformFields) {
		JSONObject fields = new JSONObject();
		
		for(TransformField transformField: transformFields) {
			JSONObject temp = new JSONObject();
			
			JSONObject _p = new JSONObject();
			_p.put("P"+RandomStringUtils.randomNumeric(10), "R");
			_p.put("P"+RandomStringUtils.randomNumeric(10), "R");
			_p.put("P"+RandomStringUtils.randomNumeric(10), "R");
			
			temp.put("_t", changeDataType(transformField.getDataType()));
			temp.put("_p", _p);
			
			fields.put(CommonService.toCamelCase(transformField.getName()), temp);
		}
		
		JSONObject _id = new JSONObject();
		JSONObject _p = new JSONObject();
		_p.put("P"+RandomStringUtils.randomNumeric(10), "R");
		_p.put("P"+RandomStringUtils.randomNumeric(10), "R");
		_p.put("P"+RandomStringUtils.randomNumeric(10), "R");
		_id.put("_t", "String");
		_id.put("_p", _p);
		
		fields.put("_id", _id);
		
		if(dataServiceName.equalsIgnoreCase("EXPTRANS")) {
			JSONObject _pTemp = new JSONObject();
			_pTemp.put("P"+RandomStringUtils.randomNumeric(10), "R");
			_pTemp.put("P"+RandomStringUtils.randomNumeric(10), "R");
			_pTemp.put("P"+RandomStringUtils.randomNumeric(10), "R");
			
			JSONObject stageStaging = new JSONObject();
			stageStaging.put("_t", "String");
			stageStaging.put("_p", _pTemp);
			fields.put(CommonService.toCamelCase("stage_staging"), stageStaging);
		}else if(dataServiceName.equalsIgnoreCase("SQ_Shortcut_to_DUMMY_SOURCE")) {
			JSONObject _pTemp = new JSONObject();
			_pTemp.put("P"+RandomStringUtils.randomNumeric(10), "R");
			_pTemp.put("P"+RandomStringUtils.randomNumeric(10), "R");
			_pTemp.put("P"+RandomStringUtils.randomNumeric(10), "R");
			
			JSONObject stageSor = new JSONObject();
			stageSor.put("_t", "String");
			stageSor.put("_p", _pTemp);
			fields.put(CommonService.toCamelCase("stage_sor"), stageSor);
		}else if(dataServiceName.equalsIgnoreCase("Shortcut_to_HEALTH_CHK_ERR_TBL")) {
			JSONObject _pTemp = new JSONObject();
			_pTemp.put("P"+RandomStringUtils.randomNumeric(10), "R");
			_pTemp.put("P"+RandomStringUtils.randomNumeric(10), "R");
			_pTemp.put("P"+RandomStringUtils.randomNumeric(10), "R");
			
			JSONObject stageLanding = new JSONObject();
			stageLanding.put("_t", "String");
			stageLanding.put("_p", _pTemp);
			fields.put(CommonService.toCamelCase("stage_landing"), stageLanding);
		}
		
		return fields;
	}
	
	
	@SuppressWarnings("unchecked")
	public static JSONObject createFieldsForUpdate(List<TransformField> transformFields, List<Connector> connector) {
		JSONObject fields = new JSONObject();
		
		for(TransformField transformField: transformFields) {
			JSONObject _updateField = new JSONObject();
			JSONObject _id2 = new JSONObject();
			JSONObject _p2 = new JSONObject();
			_id2.put("_t", changeDataType(transformField.getDataType()));
			_id2.put("_p", _p2);
			_updateField.put("_id", _id2);
			fields.put(toCamelCase(transformField.getName()), _updateField);
		}
		
		JSONObject _id = new JSONObject();
		JSONObject _p = new JSONObject();
		_id.put("_t", "String");
		_id.put("_p", _p);
		fields.put("_id", _id);
		
		JSONObject _pTemp = new JSONObject();
		_pTemp.put("P"+RandomStringUtils.randomNumeric(10), "R");
		_pTemp.put("P"+RandomStringUtils.randomNumeric(10), "R");
		_pTemp.put("P"+RandomStringUtils.randomNumeric(10), "R");
		
		JSONObject stageStaging = new JSONObject();
		stageStaging.put("_t", "String");
		stageStaging.put("_p", _pTemp);
		fields.put(CommonService.toCamelCase("stage_staging"), stageStaging);

		return fields;
	}
	
	
	public static String changeDataType(String dataType) {
		switch (dataType) {
		case "string":
			return "String";	
		case "integer":
			return "Number";
		case "bigint":
			return "Number";
		case "date/time":
			return "Date";
		}
		return dataType;
	}
}
