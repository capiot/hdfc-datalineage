package com.capiot.hdfcxmlreader.service;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.capiot.hdfcxmlreader.model.TransformField;
import com.capiot.hdfcxmlreader.model.Transformation;

@Component
public class TransformationService {
	
	@Autowired
	Environment environment;
	
	@SuppressWarnings("unchecked")
	public JSONObject createJSONTransformation(Transformation transformation) {

		JSONObject request = new JSONObject();
		request.put("name", transformation.getName());
		request.put("description", null);
		request.put("api", "/"+transformation.getName().replace("_", "").toLowerCase());
		request.put("version", 1);
		request.put("domain", environment.getProperty("domain"));
		
		JSONObject definition = new JSONObject();
		List<TransformField> transformfields = transformation.getTransformFields();
		for(TransformField transformfield: transformfields) {
			JSONObject tempField = new JSONObject();
			
			JSONObject properties = new JSONObject();
			properties.put("name", transformfield.getName());
			properties.put("_typeChanged", CommonService.changeDataType(transformfield.getDataType()));
			
			tempField.put("type", CommonService.changeDataType(transformfield.getDataType()));
			tempField.put("properties", properties);
			
			definition.put(CommonService.toCamelCase(transformfield.getName()), tempField);
		}
		JSONObject _id = new JSONObject();
		_id.put("prefix", transformation.getName().toUpperCase().substring(0,3));
		_id.put("suffix", null);
		_id.put("padding", null);
		_id.put("counter", 1001);
		_id.put("isPermanentDelete", true);
		JSONObject propertiesOfId = new JSONObject();
		propertiesOfId.put("name", "ID");
		_id.put("properties", propertiesOfId);
		definition.put("_id", _id);
		
		if(transformation.getName().equalsIgnoreCase("EXPTRANS")) {
			JSONObject properties = new JSONObject();
			properties.put("name", "stage-staging");
			properties.put("_typeChanged", "String");
			
			JSONObject stage_staging = new JSONObject();
			stage_staging.put("type", "String");
			stage_staging.put("properties", properties);
			definition.put(CommonService.toCamelCase("stage_staging"), stage_staging);
		}else if(transformation.getName().equalsIgnoreCase("SQ_Shortcut_to_DUMMY_SOURCE")) {
			JSONObject properties = new JSONObject();
			properties.put("name", "stage-sor");
			properties.put("_typeChanged", "String");
			
			JSONObject stage_sor = new JSONObject();
			stage_sor.put("type", "String");
			stage_sor.put("properties", properties);
			definition.put(CommonService.toCamelCase("stage_sor"), stage_sor);
		}
		
		
		JSONArray attributeList = new JSONArray();
		for(TransformField transformfield:transformfields) {
			JSONObject tempAttribute = new JSONObject();
			tempAttribute.put("key", CommonService.toCamelCase(transformfield.getName()));
			tempAttribute.put("name", transformfield.getName());
			
			attributeList.add(tempAttribute);
		}
		JSONObject idAttribute = new JSONObject();
		idAttribute.put("key", "_id");
		idAttribute.put("name", "ID");
		attributeList.add(idAttribute);
		
		if(transformation.getName().equalsIgnoreCase("EXPTRANS")) {
			JSONObject stageStaging = new JSONObject();
			stageStaging.put("key", CommonService.toCamelCase("stage_staging"));
			stageStaging.put("name", "stage-staging");
			attributeList.add(stageStaging);
		}else if(transformation.getName().equalsIgnoreCase("SQ_Shortcut_to_DUMMY_SOURCE")) {
			JSONObject stageSor = new JSONObject();
			stageSor.put("key", CommonService.toCamelCase("stage_sor"));
			stageSor.put("name", "stage-sor");
			attributeList.add(stageSor);
		}
		
		JSONObject versionValidity = new JSONObject();
		versionValidity.put("validityType", "count");
		versionValidity.put("validityValue", -1);
		
		request.put("definition", definition);
		request.put("attributeList", attributeList);
		request.put("versionValidity", versionValidity);
		request.put("role", CommonService.createDefaultRoles(transformation.getName(), transformfields));
		
		return request;
	}
}
