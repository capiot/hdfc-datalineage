package com.capiot.hdfcxmlreader.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capiot.hdfcxmlreader.exception.InvalidRequestException;
import com.capiot.hdfcxmlreader.model.Connector;
import com.capiot.hdfcxmlreader.model.Folder;
import com.capiot.hdfcxmlreader.model.Instance;
import com.capiot.hdfcxmlreader.model.Mapping;
import com.capiot.hdfcxmlreader.model.PowerMart;
import com.capiot.hdfcxmlreader.model.Transformation;

@RestController
@Configuration
@PropertySource("classpath:hdfc.properties")
public class XMLReaderService {
	
	@Autowired
	CommonService commonService;
	@Autowired
	ODPClient odpClient;
	@Autowired
	TransformationService transformationService;
	@Autowired
	InstanceService instanceService;
	@Autowired
	ConnectorService connectorService;
	@Autowired
	Environment environment;
	
	public static final Logger logger = LoggerFactory.getLogger(XMLReaderService.class);
	
	Map<String,String> relationMap = new HashMap<>();
	Mapping genericHealthCheckMapping = null;
	List<Transformation> transformations = null;
	
	
	/**
	 * <p>Creates the Data services 
	 * in the ODP</p>
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@GetMapping(value="/api/createDataServices")
	public JSONObject createServices() throws Exception {
		JSONObject responseJSON = new JSONObject();
		PowerMart powerMart = commonService.unmarshalXML(environment.getProperty("filePath"));
		
		List<Folder> folders = powerMart.getRepository().getFolders();
		Folder hdfcMartCBFolder = null;
		for(Folder folder: folders) {
			if(folder.getName().equalsIgnoreCase("HDFC_MART_CB")) {
				hdfcMartCBFolder = folder;
			}
		}
		
		List<Mapping> mappings = hdfcMartCBFolder.getMapping();
		
		for(Mapping mapping: mappings) {
			if(mapping.getName().equalsIgnoreCase("m_Generic_Health_Check")) {
				genericHealthCheckMapping=mapping;
			}
		}
		
		/**
		 * <p>Creating Data services for Transformation tag</p>
		 */
		transformations = genericHealthCheckMapping.getTranformations();
		for(Transformation transformation: transformations) {
			JSONObject transformationJSON = transformationService.createJSONTransformation(transformation);
			logger.info(transformationJSON.toJSONString());
			ResponseEntity<JSONObject> response = odpClient.createDataService(transformationJSON);
			if(response.getStatusCode()==HttpStatus.ACCEPTED || response.getStatusCode()==HttpStatus.OK) {
				logger.info(transformation.getName()+" Data Service has been successfully created");
			}else {
				logger.info("Unable to create the Data Service {}", transformation.getName());
				throw new Exception("Unable to create the Data Service {}" +transformation.getName());
			}
			relationMap.put(transformation.getName(), response.getBody().get("_id").toString());
			responseJSON.put(response.getBody().get("name").toString(), "Created successfully");
		}
		
		/**
		 * <p>Creating Data services for Instance tag</p>
		 */
		List<Instance> instances = genericHealthCheckMapping.getInstances();
		for(Instance instance: instances) {
			if(instance.getType().equals("TARGET")) {
				JSONObject instanceJSON = instanceService.createInstance(instance, transformations);
				logger.info(instanceJSON.toJSONString());
				ResponseEntity<JSONObject> response = odpClient.createDataService(instanceJSON);
				if(response.getStatusCode()==HttpStatus.ACCEPTED || response.getStatusCode()==HttpStatus.OK) {
					logger.info(instance.getName()+" Data Service has been successfully created");
				}else {
					logger.info("Unable to create the Data Service {}", instance.getName());
				}
				relationMap.put(instance.getName(), response.getBody().get("_id").toString());
				responseJSON.put(response.getBody().get("name").toString(), "Created successfully");
			}
		}
		
		return responseJSON;
	}
	
	
	
	/**
	 * <p> Creates the relations
	 * between the data services
	 * 
	 * @throws InvalidRequestException
	 */
	@SuppressWarnings("unchecked")
	@GetMapping(value="/api/createRelations")
	public JSONObject createRelations() throws InvalidRequestException {	
		JSONObject responseJSON = new JSONObject();
		List<Connector> allConnectors = genericHealthCheckMapping.getConnectors();
		List<Connector> sqShortcutToDummySourceConncectors = new ArrayList<>();
		List<Connector> exptrans = new ArrayList<>();
			for(Connector connector: allConnectors) {
				if(connector.getToInstanceType().equalsIgnoreCase("Target Definition")) {
					if(connector.getFromInstance().equalsIgnoreCase(transformations.get(0).getName())) {
						sqShortcutToDummySourceConncectors.add(connector);
					}else if(connector.getFromInstance().equalsIgnoreCase(transformations.get(1).getName())) {
						exptrans.add(connector);
					}
				}
			}
			
			if(!sqShortcutToDummySourceConncectors.isEmpty()) {
				JSONObject connectorJSON = connectorService.createJSONConnector(sqShortcutToDummySourceConncectors, relationMap, transformations);
				logger.info(connectorJSON.toJSONString());
				ResponseEntity<JSONObject> response = odpClient.updateDataService(connectorJSON, relationMap.get(transformations.get(0).getName()));
				if(response.getStatusCode()==HttpStatus.ACCEPTED || response.getStatusCode()==HttpStatus.OK) {
					logger.info(response.getBody().get("name")+" relation has been successfully created");
				}else {
					logger.info("Unable to create the relation");
				}
				responseJSON.put(response.getBody().get("name").toString(), "Created successfully");
			}
			
			if(!exptrans.isEmpty()) {
				JSONObject connectorJSON = connectorService.createJSONConnector(exptrans, relationMap, transformations);
				logger.info(connectorJSON.toJSONString());
				ResponseEntity<JSONObject> response = odpClient.updateDataService(connectorJSON, relationMap.get(transformations.get(1).getName()));
				if(response.getStatusCode()==HttpStatus.ACCEPTED || response.getStatusCode()==HttpStatus.OK) {
					logger.info(response.getBody().get("name")+" relation has been successfully created");
				}else {
					logger.info("Unable to create the relation");
				}
				responseJSON.put(response.getBody().get("name").toString(), "Created successfully");
			}
		
		return responseJSON;
	}
}
