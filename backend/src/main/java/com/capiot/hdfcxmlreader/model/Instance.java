package com.capiot.hdfcxmlreader.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="TRANSFORMATION")
@XmlAccessorType(XmlAccessType.FIELD)
public class Instance {
	
	@XmlAttribute(name="NAME")
	String name; 
	
	@XmlAttribute(name="TYPE")
	String type;

	public Instance(String type, String name) {
		super();
		this.type = type;
		this.name = name;
	}

	public Instance() {
		super();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
