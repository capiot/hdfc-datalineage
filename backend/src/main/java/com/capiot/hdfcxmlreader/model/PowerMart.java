package com.capiot.hdfcxmlreader.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="POWERMART")
@XmlAccessorType(XmlAccessType.FIELD)
public class PowerMart {
	
	@XmlElement(name="REPOSITORY")
	Repository repository;

	public PowerMart(Repository repository) {
		super();
		this.repository = repository;
	}

	public PowerMart() {
		super();
	}

	public Repository getRepository() {
		return repository;
	}

	public void setRepository(Repository repository) {
		this.repository = repository;
	}
	
	
}
