package com.capiot.hdfcxmlreader.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="TRANSFORMATION")
@XmlAccessorType(XmlAccessType.FIELD)
public class Transformation {
	
	@XmlAttribute(name="NAME")
	String name;
	
	@XmlElement(name="TRANSFORMFIELD")
	List<TransformField> transformFields;

	public Transformation(String name, List<TransformField> transformFields) {
		super();
		this.name = name;
		this.transformFields = transformFields;
	}

	public Transformation() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<TransformField> getTransformFields() {
		return transformFields;
	}

	public void setTransformFields(List<TransformField> transformFields) {
		this.transformFields = transformFields;
	}
		
	
}
