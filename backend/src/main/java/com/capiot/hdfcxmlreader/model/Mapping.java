package com.capiot.hdfcxmlreader.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="MAPPING")
@XmlAccessorType(XmlAccessType.FIELD)
public class Mapping {
	
	@XmlAttribute(name="NAME")
	String name;
	
	@XmlElement(name="TRANSFORMATION")
	List<Transformation> tranformations;
	
	@XmlElement(name="INSTANCE")
	List<Instance> instances;
	
	@XmlElement(name="CONNECTOR")
	List<Connector> connectors;
	
	public Mapping(String name, List<Transformation> tranformations, List<Instance> instances, List<Connector> connectors) {
		super();
		this.name = name;
		this.tranformations = tranformations;
		this.instances=instances;
		this.connectors=connectors;
	}

	public Mapping() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Transformation> getTranformations() {
		return tranformations;
	}

	public void setTranformations(List<Transformation> tranformations) {
		this.tranformations = tranformations;
	}

	public List<Instance> getInstances() {
		return instances;
	}

	public void setInstances(List<Instance> instances) {
		this.instances = instances;
	}

	public List<Connector> getConnectors() {
		return connectors;
	}

	public void setConnectors(List<Connector> connectors) {
		this.connectors = connectors;
	}
	
	
}
