package com.capiot.hdfcxmlreader.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="TRANSFORMATION")
@XmlAccessorType(XmlAccessType.FIELD)
public class Connector {

	@XmlAttribute(name="FROMFIELD")
	String fromField;
	
	@XmlAttribute(name="FROMINSTANCE")
	String fromInstance;
	
	@XmlAttribute(name="TOFIELD")
	String toField;
	
	@XmlAttribute(name="TOINSTANCE")
	String toInstance;
	
	@XmlAttribute(name="TOINSTANCETYPE")
	String toInstanceType;

	public Connector(String fromField, String fromInstance, String toField, String toInstance, String toInstanceType) {
		super();
		this.fromField = fromField;
		this.fromInstance = fromInstance;
		this.toField = toField;
		this.toInstance = toInstance;
		this.toInstanceType = toInstanceType;
	}

	public Connector() {
		super();
	}

	public String getFromField() {
		return fromField;
	}

	public void setFromField(String fromField) {
		this.fromField = fromField;
	}

	public String getFromInstance() {
		return fromInstance;
	}

	public void setFromInstance(String fromInstance) {
		this.fromInstance = fromInstance;
	}

	public String getToField() {
		return toField;
	}

	public void setToField(String toField) {
		this.toField = toField;
	}

	public String getToInstance() {
		return toInstance;
	}

	public void setToInstance(String toInstance) {
		this.toInstance = toInstance;
	}

	public String getToInstanceType() {
		return toInstanceType;
	}

	public void setToInstanceType(String toInstanceType) {
		this.toInstanceType = toInstanceType;
	}
	
	
}
