package com.capiot.hdfcxmlreader.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="FOLDER")
@XmlAccessorType(XmlAccessType.FIELD)
public class Folder {
	
	@XmlAttribute(name="NAME")
	String name;
	
	@XmlElement(name="MAPPING")
	List<Mapping> mapping;

	public Folder() {
		super();
	}

	public Folder(String name, List<Mapping> mapping) {
		super();
		this.name = name;
		this.mapping = mapping;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Mapping> getMapping() {
		return mapping;
	}

	public void setMapping(List<Mapping> mapping) {
		this.mapping = mapping;
	}

	
}
