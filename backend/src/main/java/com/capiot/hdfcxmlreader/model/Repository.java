package com.capiot.hdfcxmlreader.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="REPOSITORY")
@XmlAccessorType(XmlAccessType.FIELD)
public class Repository {
	
	@XmlElement(name="FOLDER")
	List<Folder> folders;

	public Repository(List<Folder> folders) {
		super();
		this.folders = folders;
	}

	public Repository() {
		super();
	}

	public List<Folder> getFolders() {
		return folders;
	}

	public void setFolders(List<Folder> folders) {
		this.folders = folders;
	}
	
	
}
