package com.capiot.hdfcxmlreader.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement(name="TRANSFORMFIELD")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransformField {
	
	@XmlAttribute(name="NAME")
	String name;
	
	@XmlAttribute(name="DATATYPE")
	String dataType;

	public TransformField(String name, String dataType) {
		super();
		this.name = name;
		this.dataType = dataType;
	}

	public TransformField() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	
}
