package com.capiot.hdfcxmlreader.exception;

import org.springframework.stereotype.Component;

@Component
public class InvalidRequestException extends Exception{
	private static final long serialVersionUID = 1L;
	
	public InvalidRequestException(String exception) {
		super(exception);
	}
	
	public InvalidRequestException(String exception, Throwable cause) {
		super(exception, cause);
	}

	public InvalidRequestException() {
		super();
	}
}
